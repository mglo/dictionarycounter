﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DictGenerator
{
    public partial class Form1 : Form
    {
        String[] alphabetCapitalArray = { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" };
        String[] alphabetSmallArray = { "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z" };
        String[] numbersArray = { "1", "2", "3", "4", "5", "6", "7", "8", "9", "0" };
        String[] specialCharacters = { "!", "@", "#", "$", "%", "^", "&", "*", "(", ")", "_", "-", "+", "=" };

        public Form1()
        {
            InitializeComponent();

        }


        //  1. Obliczanie kombinacji bez powtórzeń
        //wzór: n!/k! (n-k)!
        //!- silnia- liczba która jest mnożona do zera czyli 6! to 6 razy 5 razy 4 razy 3 itd.

        //Przykład: Jakie prawdopodobieństwo jest że wygramy w "Lotto". Losowane jest 6z 49 liczb i teraz wystarczy dać liczby pod wzór:
        //49!/6!(49-6)!

        private void button1_Click(object sender, EventArgs e)
        {

            if (!string.IsNullOrWhiteSpace(textBox1.Text))
            {

                int templateCharSize = 0;

                if (checkBoxNumber.Checked)
                {
                    templateCharSize += numbersArray.Length;
                }
                if (checkBoxCapitalLeters.Checked)
                {
                    templateCharSize += alphabetCapitalArray.Length;
                }
                if (checkBoxLowerCase.Checked)
                {
                    templateCharSize += alphabetSmallArray.Length;
                }
                if (checkBoxCharacters.Checked)
                {
                    templateCharSize += specialCharacters.Length;
                }

                if (templateCharSize != 0)
                {
                    int phraseSize = int.Parse(textBox1.Text);
                    double upper = silnia(templateCharSize);

                    double lowerF = silnia(phraseSize);
                    double lowerL = silnia(templateCharSize - phraseSize);
                    double Combinations = upper / (lowerF * lowerL);

                    labelStatus.Text = "number of passwords: " + Combinations.ToString();
                }
                else
                {
                    labelStatus.Text = "No parameters selected";
                }
        
            }
            else
            {
                labelStatus.Text = "Provide passowrd lenght";
            }
          
        }


        private double silnia(int liczba)
        {
            double result = 1;
            for(int i = 1; i <= liczba; i++)
            {
                result *= i;
            }

            return result;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            //TODOs generating string table with passwords without duplications
            String[] lines = { "QWESADZXC", "QWEASDOIU" };
            saveTotxt(lines);


        }

        private void saveTotxt(String[] table)
        {
            //TODOs saving to file array content

            string filepath = @"C:\\temp\text.txt";
            System.IO.StreamWriter file = new System.IO.StreamWriter(filepath);
            for(int i = 0; i < table.Length; i++)
            {
                file.WriteLine(table[i]);
            }
          
            file.Close();

        }

    }
}
